/*
 * 
 * This is Jemula.
 *
 *    Copyright (c) 2016 Stefan Mangold
 *    All rights reserved. Urheberrechtlich geschuetzt.
 *    
 *    Redistribution and use in source and binary forms, with or without modification,
 *    are permitted provided that the following conditions are met:
 *    
 *      Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer. 
 *    
 *      Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution. 
 *    
 *      Neither the name of any affiliation of Stefan Mangold nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission. 
 *    
 *    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *    IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 *    OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *    OF SUCH DAMAGE.
 *    
 */

package util;

/**
 * A helper class with vector operations in both the Cartesian and the
 * geographical coordinate system
 * 
 * @author friggr
 * 
 */
public class Vector3d {
  public static final double earthRadius = 6367444.25;

  private double x, y, z;

  public Vector3d() {
  }

  public Vector3d(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  public Vector3d clone() {
    return new Vector3d(x, y, z);
  }

  public Vector3d add(Vector3d otherVector) {
    Vector3d result = this.clone();
    result.x += otherVector.getX();
    result.y += otherVector.getY();
    result.z += otherVector.getZ();
    return result;
  }

  public Vector3d sub(Vector3d otherVector) {
    Vector3d result = this.clone();
    result.x -= otherVector.getX();
    result.y -= otherVector.getY();
    result.z -= otherVector.getZ();
    return result;
  }

  public double dot(Vector3d otherVector) {
    return x * otherVector.getX() + y * otherVector.getY() + z * otherVector.getZ();
  }

  public Vector3d cross(Vector3d otherVector) {
    return new Vector3d(y * otherVector.getZ() - z * otherVector.getY(), z * otherVector.getX() - x * otherVector.getZ(), x
        * otherVector.getY() - y * otherVector.getX());
  }

  public Vector3d scale(double factor) {
    return new Vector3d(x * factor, y * factor, z * factor);
  }

  public Vector3d reflect() {
    return new Vector3d(-x, -y, -z);
  }

  public double getDistanceTo(Vector3d otherVector) {
    return otherVector.sub(this).getLength();
  }

  public double getAngleTo(Vector3d otherVector) {
    return Math.acos(this.normalize().dot(otherVector.normalize()));
  }

  public Vector3d normalize() {
    if (this.getLength() == 0) {
      return this;
    }
    return this.scale(1 / this.getLength());
  }

  public Vector3d rotate(double angleDegrees, int axis) {
    Vector3d res = this.clone();
    double cosAngle = Math.cos(Math.toRadians(angleDegrees));
    double sinAngle = Math.sin(Math.toRadians(angleDegrees));
    switch (axis) {
      case 0:
        res.setY(y * cosAngle + z * -sinAngle);
        res.setZ(y * sinAngle + z * cosAngle);
        return res;
      case 1:
        res.setX(x * cosAngle + z * sinAngle);
        res.setZ(x * -sinAngle + z * cosAngle);
        return res;
      case 2:
        res.setX(x * cosAngle + y * -sinAngle);
        res.setY(x * sinAngle + y * cosAngle);
        return res;
      default:
        return res;
    }
  }

  public double getLength() {
    return Math.sqrt(x * x + y * y + z * z);
  }

  public double getX() {
    return x;
  }

  public void setX(double x) {
    this.x = x;
  }

  public double getY() {
    return y;
  }

  public void setY(double y) {
    this.y = y;
  }

  public double getZ() {
    return z;
  }

  public void setZ(double z) {
    this.z = z;
  }

  public double getLat() {
    double length = getLength();
    if (length != 0) {
      return Math.toDegrees(Math.asin(z / getLength()));
    } else {
      return 0;
    }
  }

  public void setLatLong(double lat, double lon) {
    // http://stackoverflow.com/questions/1185408/converting-from-longitude-latitude-to-cartesian-coordinates
    x = Vector3d.earthRadius * Math.cos(Math.toRadians(lat)) * Math.cos(Math.toRadians(lon));
    y = Vector3d.earthRadius * Math.cos(Math.toRadians(lat)) * Math.sin(Math.toRadians(lon));
    z = Vector3d.earthRadius * Math.sin(Math.toRadians(lat));
  }

  public double getLon() {
    return Math.toDegrees(Math.atan2(y, x));
  }

  public double getAlt() {
    return getLength() - earthRadius;
  }

  public void setAlt(double alt) {
    double oldLength = getLength();
    double newLength = alt + earthRadius;
    if (oldLength != 0) {
      x = x / oldLength * newLength;
      y = y / oldLength * newLength;
      z = z / oldLength * newLength;
    } else {
      z = alt + earthRadius;
    }
  }

  @Override
  public String toString() {
    return "X: " + x + " Y: " + y + " Z:" + z;
  }

  public String toStringGeo() {
    return "Lat: " + getLat() + "Lon: " + getLon() + "Alt: " + getAlt();
  }
}

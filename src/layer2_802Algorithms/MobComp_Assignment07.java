package layer2_802Algorithms;

import plot.JEMultiPlotter;
import layer1_802Phy.JE802PhyMode;
import layer2_80211Mac.JE802_11BackoffEntity;
import layer2_80211Mac.JE802_11Mac;
import layer2_80211Mac.JE802_11MacAlgorithm;

public class MobComp_Assignment07 extends JE802_11MacAlgorithm {
	
	private JE802_11BackoffEntity theBackoffEntityAC01;
	
	private double theSamplingTime_sec;
	
	public MobComp_Assignment07(String name, JE802_11Mac mac) {
		super(name, mac);
		this.theBackoffEntityAC01 = this.mac.getBackoffEntity(1);
		message("This is station " + this.dot11MACAddress.toString() +". MobComp algorithm: '" + this.algorithmName + "'.", 100);
	}
	
	@SuppressWarnings("unused")
	@Override
	public void compute() {

		this.mac.getMlme().setTheIterationPeriod(0.1);  // the sampling period in seconds, which is the time between consecutive calls of this method "compute()"
		this.theSamplingTime_sec =  this.mac.getMlme().getTheIterationPeriod().getTimeS(); // this sampling time can only be read after the MLME was constructed.
		
		this.theSamplingTime_sec = this.theSamplingTime_sec + 0;
		
		
		// observe outcome:  (might need to be stored from iteration to iteration)
		int aQueueSize = this.theBackoffEntityAC01.getQueueSize();
		int aCurrentQueueSize = this.theBackoffEntityAC01.getCurrentQueueSize();
		double aCurrentTxPower_dBm = this.mac.getPhy().getCurrentTransmitPower_dBm();
		JE802PhyMode aCurrentPhyMode = this.mac.getPhy().getCurrentPhyMode();

		// --------------------- ASSIGNMENT - add your PID controller here:

		// Possible Phy modes: "BPSK12", "BPSK34", "QPSK12", "QPSK34", "16QAM12", "16QAM34", "64QAM23", "64QAM34"
    
		// this.mac.getPhy().setCurrentPhyMode("BPSK12");   // it is possible to change the PhyMode
		// this.mac.getPhy().setCurrentPhyMode("64QAM34");   // it is possible to change the PhyMode
		this.mac.getPhy().setCurrentPhyMode("16QAM34");   // it is possible to change the PhyMode
		this.mac.getPhy().setCurrentTransmitPower_dBm(0); // it is also possible to change the transmission power (please not higher than 0dBm)

		// --------------------- ASSIGNMENT -------------------------------
	}
	
	@Override
	public void plot() {
		if (plotter == null) {
			plotter = new JEMultiPlotter("PID Controller, Station " + this.dot11MACAddress.toString(), "max", "time [s]", "MAC Queue", this.theUniqueEventScheduler.getEmulationEnd().getTimeMs() / 1000.0, true);
			plotter.addSeries("current");
			plotter.display();
		}
		plotter.plot(((Double) theUniqueEventScheduler.now().getTimeMs()).doubleValue() / 1000.0, theBackoffEntityAC01.getQueueSize(), 0);
		plotter.plot(((Double) theUniqueEventScheduler.now().getTimeMs()).doubleValue() / 1000.0, theBackoffEntityAC01.getCurrentQueueSize(), 1);
	}

}

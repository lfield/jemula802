package layer2_802Algorithms;

import layer2_80211Mac.JE802_11Mac;
import layer2_80211Mac.JE802_11MacAlgorithm;

import statistics.JERandomVar;

public class random extends JE802_11MacAlgorithm {
	
	private JERandomVar theRandomVar;

	public random(String name, JE802_11Mac mac) {
		super(name, mac);
		this.theRandomVar = new JERandomVar(this.theUniqueRandomGenerator, "Uniform", 0.0, 10.0);
	}
	
	@Override
	public void compute() {
		// TODO Auto-generated method stub
		message("random number between 0 and 10: " + this.theRandomVar.nextvalue());
	}
	
	@Override
	public void plot() {
		// TODO Auto-generated method stub
		
	}

}

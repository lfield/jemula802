package layer2_802Algorithms;

import layer2_80211Mac.JE802_11BackoffEntity;
import layer2_80211Mac.JE802_11Mac;
import layer2_80211Mac.JE802_11MacAlgorithm;

public class ShutDown extends JE802_11MacAlgorithm {

	private JE802_11BackoffEntity theBackoffEntityAC01;

	public ShutDown(String name, JE802_11Mac mac) {
		super(name, mac);
		this.theBackoffEntityAC01 = this.mac.getBackoffEntity(1);
	}

	@Override
	public void compute() {
		message("Shutting down the backoff entity " + theBackoffEntityAC01.getAC().toString() + " now!");
		theBackoffEntityAC01.shutDown();
		this.mac.getMlme().disableComputing(); // shutting myself down.
	}

	@Override
	public void plot() {
	}
}

/*
 * 
 * This is Jemula.
 *
 *    Copyright (c) 2009 Stefan Mangold, Fabian Dreier, Stefan Schmid
 *    All rights reserved. Urheberrechtlich geschuetzt.
 *    
 *    Redistribution and use in source and binary forms, with or without modification,
 *    are permitted provided that the following conditions are met:
 *    
 *      Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer. 
 *    
 *      Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution. 
 *    
 *      Neither the name of any affiliation of Stefan Mangold nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission. 
 *    
 *    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *    IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 *    OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *    OF SUCH DAMAGE.
 *    
 */

package emulator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileDeleteStrategy;
import org.w3c.dom.Document;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class JE802Starter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("This is Jemula802. Argument missing. Please provide the XML scenario file.");
			return;
		}
		String aScenarioFile = args[0];
		JE802Control control = new JE802Control(parseDocument(aScenarioFile));
		copyFile(aScenarioFile, control.getPath2Results());
		control.emulate(); // run the show (by starting the scheduler)
		control.visualize(); // create google earth animation, if required
		System.out.println(" This is Jemula802. All done.\n"); // end, all done, shutting down.
	}

	private static Document parseDocument(String aScenarioFilename) {
		Document anXMLdoc = null;
		File theScenarioFile = new File(aScenarioFilename); // The file to parse
		if (!theScenarioFile.exists()) { // the XML scenario file does not exist or is not accessible
			System.err.println(
					"This is Jemula802. Error: could not open the XML scenario file " + theScenarioFile);
			System.exit(0);
		}

		System.out.println(" This is Jemula802. Simulating scenario \""
				+ theScenarioFile.getName() + "\"...");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(true);
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			anXMLdoc = builder.parse(theScenarioFile);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("JE802Starter: error when parsing XML scenario file.");
		}
		if (anXMLdoc == null) {
			System.err.println("JE802Starter: error when parsing XML scenario file, document null.");			
		}
		return anXMLdoc;
	}

	public static void copyDir(Path aSrcDirectory, Path aDestDirectory) throws IOException {
		Files.walk(aSrcDirectory).forEach(source -> {

			if (source == aSrcDirectory) {
				// do nothing
			} else {
				try {
					Path target = Paths.get(aDestDirectory + File.separator + source.getFileName());
					if (Files.exists(target)) {
						removeFile(target.toString());
					}
					Files.copy(source, target, REPLACE_EXISTING);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void copyFile(String aSourceFile, String aDestDirectory) {
		Path source = Paths.get(aSourceFile);
		Path target = Paths.get(aDestDirectory + File.separator + source.getFileName());

		try {
			if (!Files.exists(Paths.get(aDestDirectory))) {
				Files.createDirectories(Paths.get(aDestDirectory));
			}
			if (!Files.isDirectory(source)) {
				Files.copy(source, target, REPLACE_EXISTING);
			} else {
				if (!Files.exists(target)) {
					Files.createDirectories(target);
				}
				copyDir(source, target);
			}
		} catch (Exception e) {
			System.err.println(" Cannot copy file " + aSourceFile);
			e.printStackTrace();
		}
	}

	public static void removeFile(String sourceFile) {
		File aFile = new File(sourceFile);
		try {
			FileDeleteStrategy.FORCE.delete(aFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
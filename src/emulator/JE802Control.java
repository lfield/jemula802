/*
 * 
 * This is Jemula.
 *
 *    Copyright (c) 2009 Stefan Mangold, Fabian Dreier, Stefan Schmid
 *    All rights reserved. Urheberrechtlich geschuetzt.
 *    
 *    Redistribution and use in source and binary forms, with or without modification,
 *    are permitted provided that the following conditions are met:
 *    
 *      Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer. 
 *    
 *      Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution. 
 *    
 *      Neither the name of any affiliation of Stefan Mangold nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission. 
 *    
 *    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *    IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 *    OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *    OF SUCH DAMAGE.
 *    
 */

package emulator;

import gui.JE802Gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.w3c.dom.Node;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import kernel.JEEvent;
import kernel.JEEventScheduler;
import kernel.JETime;
import kernel.JEmula;
import layer0_medium.JEWirelessMedium;

import layer1_802Phy.JE802PhyMode;
import layer3_network.JE802RoutingConstants;

import station.JE802Station;
import visualization.JE802EarthVisualization;

/**
 * @author Stefan Mangold
 *
 */
public class JE802Control extends JEmula {

	private List<JE802Station> theListOfAllStations;

	private String thePath2Results;

	private JEEventScheduler theUniqueEventScheduler;

	private JEWirelessMedium theUniqueWirelessMedium;

	private Random theUniqueRandomBaseGenerator;

	private JE802Gui theUniqueGui;

	private JE802StatEval theUniqueStatisticalEvaluation;

	private JE802EarthVisualization theUniqueEarthVisualizer;

	private Document theXmlScenario;

	private List<JE802PhyMode> theListOfPhyModes;

	private boolean visualize;

	public JE802Control(Document aDocument) {
		this.theXmlScenario = aDocument;
		this.theUniqueEventScheduler = new JEEventScheduler();
		this.theUniqueEarthVisualizer = null;
		this.theListOfPhyModes = new ArrayList<JE802PhyMode>();
		this.theListOfAllStations = new ArrayList<JE802Station>(); // queue of theStations
		this.parse_xml_and_create_entities();
		this.thePath2Results = theUniqueStatisticalEvaluation.getPath2Results();
		this.theUniqueEventScheduler.setPath2Results(this.thePath2Results);
	}

	public void emulate() {
		this.theUniqueEventScheduler.start();
	}

	public void visualize() {
		if (this.visualize) {
			theUniqueEarthVisualizer.createDOM();
		}
		this.theXmlScenario = null;
	}

	public String getPath2Results() {
		return this.thePath2Results;
	}

	private void parse_xml_and_create_entities() {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {

			Element aRootElement = theXmlScenario.getDocumentElement();
			this.theUniqueGui = createGui(aRootElement);
			this.theUniqueStatisticalEvaluation = createControlElement(aRootElement, xpath);
			this.theUniqueWirelessMedium = createWirelessChannels(aRootElement, xpath);
			this.theUniqueEarthVisualizer = createEarthVisualizer(aRootElement, xpath);
			this.theListOfPhyModes = createPhyModes(aRootElement, xpath);
			this.theUniqueStatisticalEvaluation.setPhyModes(this.theListOfPhyModes);
			setRoutingConstants(aRootElement, xpath);
			this.createStations(aRootElement, xpath);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
	}

	protected JE802Gui createGui(Element aRootElement) throws XPathExpressionException {
		Element je802ControlElement = (Element) aRootElement.getElementsByTagName("JE802Control").item(0);
		boolean showGui = Boolean.valueOf(je802ControlElement.getAttribute("showGui"));
		JE802Gui gui = null;
		if (showGui) {
			gui = new JE802Gui(this.theXmlScenario.getBaseURI());
			gui.setVisible(true);
			gui.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		}
		return gui;
	}

	protected void setRoutingConstants(Element aRootElement, XPath xpath) throws XPathExpressionException {
		Element routingParameters = (Element) xpath.evaluate("JE802RoutingParameters", aRootElement,
				XPathConstants.NODE);

		if (routingParameters == null) {
			this.warning("No <JE802RoutingParameters> in XML, using default values");
			return;
		}

		String enabledStr = routingParameters.getAttribute("routingEnabled");
		if (!enabledStr.isEmpty()) {
			JE802RoutingConstants.routingEnabled = Boolean.valueOf(enabledStr);
		}

		// dynamicChannelSwitching enabled
		String switchingStr = routingParameters.getAttribute("channelSwitchingEnabled");
		if (!switchingStr.isEmpty()) {
			JE802RoutingConstants.channelSwitchingEnabled = Boolean.valueOf(switchingStr);
		}

		String mcrMetricStr = routingParameters.getAttribute("multiChannelPathMetricEnabled");
		if (!mcrMetricStr.isEmpty()) {
			JE802RoutingConstants.MCRMetricEnabled = Boolean.valueOf(mcrMetricStr);
		}

		String routeTimeout = routingParameters.getAttribute("activeRouteTimeout_ms");
		if (!routeTimeout.isEmpty()) {
			double timeOut = Double.valueOf(routeTimeout);
			JE802RoutingConstants.ACTIVE_ROUTE_TIMEOUT = new JETime(timeOut);
		}

		String ipHeaderLengthStr = routingParameters.getAttribute("ipHeaderByte");
		if (!ipHeaderLengthStr.isEmpty()) {
			int headerLength = Integer.valueOf(ipHeaderLengthStr);
			JE802RoutingConstants.IP_HEADER_BYTE = headerLength;
		}

		String brokenStr = routingParameters.getAttribute("brokenLinkAfterLoss");
		if (!brokenStr.isEmpty()) {
			int brokenAfter = Integer.valueOf(brokenStr);
			JE802RoutingConstants.LINK_BREAK_AFTER_LOSS = brokenAfter;
		} else {
			JE802RoutingConstants.LINK_BREAK_AFTER_LOSS = 3;
		}

		String ttlStr = routingParameters.getAttribute("maxTTL");
		if (!ttlStr.isEmpty()) {
			int maxTTL = Integer.valueOf(ttlStr);
			JE802RoutingConstants.maxTTL = maxTTL;
		}

		String helloIntervalStr = routingParameters.getAttribute("helloInterval_ms");
		if (!helloIntervalStr.isEmpty()) {
			double interval = Integer.valueOf(helloIntervalStr);
			JE802RoutingConstants.HELLO_INTERVAL_MS = new JETime(interval);
		}

		String channelDelayStr = routingParameters.getAttribute("channelSwitchingDelay_ms");
		if (!channelDelayStr.isEmpty()) {
			double delay = Double.valueOf(channelDelayStr);
			JE802RoutingConstants.CHANNEL_SWITCHING_DELAY = new JETime(delay);
		}
	}

	protected void createStations(Element aRootElement, XPath xpath) throws XPathExpressionException {
		NodeList stationNodeList = (NodeList) xpath.evaluate("JE802Station", aRootElement, XPathConstants.NODESET);

		for (int i = 0; i < stationNodeList.getLength(); i++) {
			Node stationNode = stationNodeList.item(i);
			JE802Station station = new JE802Station(theUniqueEventScheduler, theUniqueWirelessMedium,
					theUniqueRandomBaseGenerator, theUniqueGui, theUniqueEarthVisualizer,
					theUniqueStatisticalEvaluation, stationNode, theListOfPhyModes);
			this.theListOfAllStations.add(station);
		}
		return;
	}

	protected List<JE802PhyMode> createPhyModes(Node theTopLevelNode, XPath xpath) throws XPathExpressionException {
		String expression = "JE802PhyModes";
		Node phyModesNode = (Node) xpath.evaluate(expression, theTopLevelNode, XPathConstants.NODE);

		List<JE802PhyMode> phyModes = new ArrayList<JE802PhyMode>();
		if (phyModesNode != null) {
			NodeList phyModeList = (NodeList) xpath.evaluate("aPhyMode", phyModesNode, XPathConstants.NODESET);
			for (int i = 0; i < phyModeList.getLength(); i++) {
				Node phyNode = phyModeList.item(i);
				phyModes.add(new JE802PhyMode(phyNode));
			}
		} else {
			this.error("No JE802PhyModes node specified in xml");
		}
		return phyModes;
	}

	protected JE802EarthVisualization createEarthVisualizer(Node theTopLevelNode, XPath xpath)
			throws XPathExpressionException {
		String expression = "//JE802Animation";
		String resultPath = theUniqueStatisticalEvaluation.getPath2Results();
		Element animationNode = (Element) xpath.evaluate(expression, theTopLevelNode, XPathConstants.NODE);
		this.visualize = Boolean.valueOf(animationNode.getAttribute("generateGoogleEarth"));
		// always instantiate, even if this.visualize=false. Some parameters are needed
		// for simulation in any case.
		JE802EarthVisualization earthVisualizer = new JE802EarthVisualization(animationNode, resultPath,
				this.theXmlScenario.getDocumentURI(), this.theListOfAllStations, this.theUniqueWirelessMedium);
		return earthVisualizer;
	}

	protected JE802StatEval createControlElement(Element aRootElement, XPath xpath) throws XPathExpressionException {
		Element je802ControlElement = (Element) aRootElement.getElementsByTagName("JE802Control").item(0);
		JE802StatEval statEval = null;
		if (je802ControlElement != null) {
			//this.theUniqueEventScheduler.setRealtime(Boolean.valueOf(je802ControlElement.getAttribute("realtime")));
			String anEmulationDuration = je802ControlElement.getAttribute("EmulationDuration_ms");
			JETime anEmulationEnd = new JETime(Double.parseDouble(anEmulationDuration));
			this.theUniqueEventScheduler.setEmulationEnd(anEmulationEnd);
			Node statEvalNode = (Node) xpath.evaluate("JE802StatEval", je802ControlElement, XPathConstants.NODE);
			if (statEvalNode != null) {
				long aSeed = Long.valueOf(((Element) statEvalNode).getAttribute("seed"));
				theUniqueRandomBaseGenerator = new Random(aSeed);
				statEval = new JE802StatEval(theUniqueEventScheduler, theUniqueRandomBaseGenerator, statEvalNode);
				statEval.send(new JEEvent("start_req", statEval, theUniqueEventScheduler.now()));
			} else {
				this.warning("No JE802StatEval node specified in xml");
			}
		} else {
			this.error("No JE802Control node specified in xml");
		}
		return statEval;
	}

	protected JEWirelessMedium createWirelessChannels(Element aRootElement, XPath xpath)
			throws XPathExpressionException {

		Node channelNode = (Node) xpath.evaluate("JEWirelessChannels", aRootElement, XPathConstants.NODE);

		JEWirelessMedium wirelessMedium = null;
		if (channelNode != null) {
			wirelessMedium = new JEWirelessMedium(theUniqueEventScheduler, theUniqueRandomBaseGenerator, channelNode);
		} else {
			this.error("No JEWirelessChannels specified in xml");
		}
		return wirelessMedium;
	}
}
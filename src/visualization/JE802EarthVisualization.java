/*
 * 
 * This is Jemula802.
 *
 *    Copyright (c) 2009 Stefan Mangold, Fabian Dreier, Stefan Schmid
 *    All rights reserved. Urheberrechtlich geschuetzt.
 * 
 *    Redistribution and use in source and binary forms, with or without modification,
 *    are permitted provided that the following conditions are met:
 * 
 *      Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * 
 *      Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution.
 * 
 *      Neither the name of any affiliation of Stefan Mangold nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 * 
 *    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *    IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 *    OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *    OF SUCH DAMAGE.
 * 
 */

package visualization;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import kernel.JEmula;
import layer0_medium.JEWirelessMedium;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.*;

import station.JE802Station;

import emulator.JE802Starter;

public class JE802EarthVisualization extends JEmula {

	// name of the result file
	private final String thePath2Results;

	private final String theFilename;

	private Document theKmlFile;

	// size of a pixel in the power/channel/address map in meters
	private final double thePixelSize_m;

	private final List<JE802Station> theListOfStations;

	private final double theMaxThrp;

	private final double theMinTx_dBm;

	private final double theMaxTx_dBm;

	private final JEWirelessMedium theUniqueWirelessMedium;

	private final double theMbpsPerBlock;

	private final double theBaseLongitude;

	private final double theBaseLatitude;

	public JE802EarthVisualization(final Node anAnimationNode, final String aPath, final String aFilename,
			final List<JE802Station> aListOfStations, final JEWirelessMedium aMedium) {

		Element animationElem = (Element) anAnimationNode;

		if (animationElem.hasAttribute("baseLongitude")) {
			this.theBaseLongitude = Double.valueOf(animationElem.getAttribute("baseLongitude"));
		} else {
			this.theBaseLongitude = Double.NaN;
			this.error("missing attribute baseLongitude in " + animationElem.getNodeName());
		}

		if (animationElem.hasAttribute("baseLatitude")) {
			this.theBaseLatitude = Double.valueOf(animationElem.getAttribute("baseLatitude"));
		} else {
			this.theBaseLatitude = Double.NaN;
			this.error("missing attribute baseLatitude in " + animationElem.getNodeName());
		}

		if (animationElem.hasAttribute("maxThrp")) {
			this.theMaxThrp = Double.valueOf(animationElem.getAttribute("maxThrp"));
		} else {
			this.theMaxThrp = Double.NaN;
			this.error("missing attribute maxThrp in " + animationElem.getNodeName());
		}

		if (animationElem.hasAttribute("maxDelay")) {
			Double.valueOf(animationElem.getAttribute("maxDelay"));
		} else {
			this.error("missing attribute maxDelay in " + animationElem.getNodeName());
		}

		if (animationElem.hasAttribute("overlayAccuracy")) {
			this.warning("Deprecated attribute: Please remove overlayAccuracy, and use PixelSize_m instead!");
		}

		if (animationElem.hasAttribute("PixelSize_m")) {
			this.thePixelSize_m = Double.valueOf(animationElem.getAttribute("PixelSize_m"));
		} else {
			this.thePixelSize_m = Double.NaN;
			this.error("missing attribute PixelSize_m in " + animationElem.getNodeName());
		}

		if (animationElem.hasAttribute("minTxdBm")) {
			this.theMinTx_dBm = Double.valueOf(animationElem.getAttribute("minTxdBm"));
		} else {
			this.theMinTx_dBm = Double.NaN;
			this.error("missing attribute minTxdBm in " + animationElem.getNodeName());
		}

		if (animationElem.hasAttribute("maxTxdBm")) {
			this.theMaxTx_dBm = Double.valueOf(animationElem.getAttribute("maxTxdBm"));
		} else {
			this.theMaxTx_dBm = Double.NaN;
			this.error("missing attribute maxTxdBm in " + animationElem.getNodeName());
		}

		if (animationElem.hasAttribute("attenuationCoeff")) {
			this.warning(
					"Deprecated attribute: Please remove attenuationCoeff. This is now defined by the wireless channel.");
		}

		String mbPBStr = new String();
		if (animationElem.hasAttribute("mbPerBlock")) {
			mbPBStr = animationElem.getAttribute("mbPerBlock");
		} else {
			this.error("missing attribute mbPerBlock in " + animationElem.getNodeName());
		}
		if (!mbPBStr.isEmpty()) {
			this.theMbpsPerBlock = Double.valueOf(mbPBStr);
		} else {
			this.theMbpsPerBlock = 0.2;
			warning("WARNING: no mbPerBlock attribute in JE802Animation tag specified, using default "
					+ this.theMbpsPerBlock);
		}

		this.theUniqueWirelessMedium = aMedium;
		this.thePath2Results = aPath;
		String[] fileParts = aFilename.split("/");
		String name = fileParts[fileParts.length - 1];
		this.theFilename = name.substring(0, name.length() - 4);
		this.theListOfStations = aListOfStations;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;

		try {
			db = dbf.newDocumentBuilder();
			this.theKmlFile = db.newDocument();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	private Element createHiddenListStyle() {
		// hidden element style of folders, just needed once in a document
		Element style = this.theKmlFile.createElement("Style");
		style.setAttribute("id", "hideChildren");
		Element listStyle = this.theKmlFile.createElement("ListStyle");
		Element listItemType = this.theKmlFile.createElement("listItemType");
		listItemType.appendChild(this.theKmlFile.createTextNode("checkHideChildren"));
		listStyle.appendChild(listItemType);
		style.appendChild(listStyle);
		return style;
	}

	private Element createFolder(final List<JE802KmlGenerator> generators, final String name,
			final boolean hideElements, final boolean isVisible) {
		Element folder = this.theKmlFile.createElement("Folder");
		Element folderName = this.theKmlFile.createElement("name");
		folderName.appendChild(this.theKmlFile.createTextNode(name));
		folder.appendChild(folderName);

		Element open = this.theKmlFile.createElement("open");
		open.appendChild(this.theKmlFile.createTextNode("1"));

		Element visibility = this.theKmlFile.createElement("visibility");
		if (isVisible) {
			visibility.appendChild(this.theKmlFile.createTextNode("1"));
		} else {
			visibility.appendChild(this.theKmlFile.createTextNode("0"));
		}
		folder.appendChild(visibility);
		folder.appendChild(open);
		for (JE802KmlGenerator gen : generators) {
			folder.appendChild(gen.createDOM());
		}
		if (hideElements) {
			Element styleUrl = this.theKmlFile.createElement("styleUrl");
			styleUrl.appendChild(this.theKmlFile.createTextNode("hideChildren"));
			folder.appendChild(styleUrl);
		}
		return folder;
	}

	/**
	 * creates the Document Object Model (DOM) of the animation XML file
	 */
	public void createDOM() {

		this.message("Generating Google Earth file (kml) ...");

		Element root = this.theKmlFile.createElement("kml");
		root.setAttribute("xmlns", "http://www.opengis.net/kml/2.2");
		root.setAttribute("xmlns:gx", "http://www.google.com/kml/ext/2.2");
		Element document = this.theKmlFile.createElement("Document");
		document.appendChild(createHiddenListStyle());
		Element open = this.theKmlFile.createElement("open");
		open.appendChild(this.theKmlFile.createTextNode("1"));
		document.appendChild(open);

		List<JE802KmlGenerator> generators = new ArrayList<JE802KmlGenerator>();

		// station models
		generators.add(new JE802StationKml(this.theKmlFile, this.theListOfStations, this.theFilename));

		// ground heatmaps (indicating radio power, or frequency channels, or MAC
		// addresses)
		generators.add(new JE802KmlGeneratorHeatMap(this.theKmlFile, this.theListOfStations, this.theFilename,
				this.thePixelSize_m, this.theMaxTx_dBm, this.theMinTx_dBm, this.theUniqueWirelessMedium,
				this.thePath2Results));

		// antennas
		generators.add(new JE802KmlGeneratorAntenna(theKmlFile, theListOfStations));

		// offer blocks
		generators.add(new JE802KmlGeneratorTrafficBlocks(this.theKmlFile, this.theListOfStations, this.theFilename,
				this.theMbpsPerBlock));

		// throughput links
		generators.add(new JE802LinksKml(this.theKmlFile, this.theListOfStations, this.theMaxThrp));

		for (JE802KmlGenerator gen : generators) {
			document.appendChild(gen.createDOM());
		}

		// backgrounds
		List<JE802KmlGenerator> aListOfBackgrounds = new ArrayList<JE802KmlGenerator>();
		aListOfBackgrounds.add(new JE802KmlGeneratorBackground(this.theKmlFile, this.theListOfStations,
				"kml" + File.separator + "white.png", "White", 3000, this.theUniqueWirelessMedium, false));
		aListOfBackgrounds.add(new JE802KmlGeneratorBackground(this.theKmlFile, this.theListOfStations,
				"kml" + File.separator + "black.png", "Black", 3000, this.theUniqueWirelessMedium, false));
		aListOfBackgrounds.add(new JE802KmlGeneratorBackground(this.theKmlFile, this.theListOfStations,
				"kml" + File.separator + "cobbles.png", "Cobbles", 400, this.theUniqueWirelessMedium, false));
		Element backgroundFolder = this.createFolder(aListOfBackgrounds, "Backgrounds", false, true);
		document.appendChild(backgroundFolder);

		this.theKmlFile.appendChild(root);
		root.appendChild(document);

		this.writeDOMtoFile();
		this.createKMZArchive();

		JE802Starter.copyFile(
				System.getProperty("user.dir") + File.separator + this.thePath2Results + File.separator + "kml"
						+ File.separator + this.theFilename + ".kml",
						System.getProperty("user.dir") + File.separator + this.thePath2Results);
		JE802Starter.removeFile(System.getProperty("user.dir") + File.separator + this.thePath2Results + File.separator
				+ "kml" + File.separator + this.theFilename + ".kml");
		JE802Starter.removeFile(System.getProperty("user.dir") + File.separator + this.thePath2Results + File.separator
				+ this.theFilename + ".kmz");
		this.message("Done.");
	}

	/**
	 * writes the Document Object Model (DOM) to a file
	 */
	private void writeDOMtoFile() {
		String destDirectory = new String(this.thePath2Results + File.separator + "kml" + File.separator);
		File directory = new File(destDirectory);
		if (!directory.exists()) { // directory does not exist
			try {
				this.message("Creating new folder " + destDirectory);
				directory.mkdirs();
			} catch (Exception e) {
				this.error("Could not create " + destDirectory);
			}
		}

		DOMImplementationRegistry registry = null;
		try {
			registry = DOMImplementationRegistry.newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | ClassCastException e) {
			e.printStackTrace();
		}
		DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("XML 3.0 LS 3.0");
		if (impl == null) {
			this.error("No DOMImplementation found !");
		}

		LSSerializer serializer = impl.createLSSerializer();
		LSOutput output = impl.createLSOutput();
		output.setEncoding("UTF-8");
		try {
			FileOutputStream aFileOutputStream = new FileOutputStream(
					destDirectory + File.separator + this.theFilename + ".kml");
			output.setByteStream(aFileOutputStream);
			serializer.write(this.theKmlFile, output);
			try {
				aFileOutputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void createKMZArchive() {
		File modelFolder = new File("resources" + File.separator + "models");
		if (!modelFolder.exists()) {
			modelFolder = new File("jemula802" + File.separator + "resources" + File.separator + "models");
		}
		File filesFolder = new File(this.thePath2Results + File.separator + "kml");
		for (File file : modelFolder.listFiles()) {
			JE802Starter.copyFile(modelFolder.getPath() + File.separator + file.getName(),
					filesFolder.getAbsolutePath());
		}

		File animationFile = new File(this.thePath2Results + File.separator + this.theFilename + ".kmz");
		FileOutputStream fileStream;
		try {
			fileStream = new FileOutputStream(animationFile);
			ZipOutputStream zipStream = new ZipOutputStream(fileStream);
			for (File file : filesFolder.listFiles()) {
				if (!file.isDirectory()) {
					FileInputStream in = new FileInputStream(file);
					ZipEntry entry = new ZipEntry(filesFolder.getAbsolutePath() + File.separator + file.getName());
					zipStream.putNextEntry(entry);
					int len;
					byte[] buf = new byte[1024];
					while ((len = in.read(buf)) > 0) {
						zipStream.write(buf, 0, len);
					}
					in.close();
					zipStream.closeEntry();
				}
			}
			zipStream.close();
			fileStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public double getTheBaseLatitude() {
		return theBaseLatitude;
	}

	public double getTheBaseLongitude() {
		return theBaseLongitude;
	}
}

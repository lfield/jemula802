function plot_thrp_offer_ETH05()

total_offer; total_thrp;

offer_SA1_DA2_AC1_ID_15_data_
thrp_SA1_DA2_AC1_ID_15_data__End;


fig_handle = figure('NumberTitle', 'on', 'Name', 'all','PaperPositionMode', 'auto','Pointer', 'arrow');

set(gcf, 'color', 1/255 * [214 221 225]);
set(gcf, 'InvertHardCopy', 'off');
set(gcf,'Renderer','painters');

set(fig_handle,'Position', [200 20 1000 600]);
set(gca,'Position',[0.08,0.14,0.9,0.82]);
set(gca,'FontSize',22);
box on;
axis on;

set(gca,"xminortick", "on")
set(gca,"xminorgrid", "on")

#ylim([0 10]);
xlim([0 max(result_total_offer(:,1)/1000)]*1.05);

ylabel('Simulation Results [Mb/s]');
xlabel('Simulated Time [s]');
grid on;
hold on;


%plot(result_offer_SA1_DA2_AC1_ID_15_data_(:,1)/1000,result_offer_SA1_DA2_AC1_ID_15_data_(:,7),':','Color',"k",'LineWidth',1);
%plot(result_thrp_SA1_DA2_AC1_ID_15_data__End(:,1)/1000,result_thrp_SA1_DA2_AC1_ID_15_data__End(:,7),':','Color',"r",'LineWidth',1);


plot(result_total_offer(:,1)/1000,result_total_offer(:,7),'-','Color',[.6 .6 .8],'LineWidth',1);
plot(result_total_thrp(:,1)/1000,result_total_thrp(:,7),'-','Color','k','LineWidth',1);

l_handle = legend("Offer [Mb/s]","Throughput [Mb/s]");
%l_handle = legend("Offer 1","Offer 2","Throughput 1","Throughput 2", "Offer overall", "Thrp overall");
set (l_handle, "FontSize", 22, "Color", [0.9 0.9 0.9], "Location", "SouthWest");

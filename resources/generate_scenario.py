# Insert XML data with placeholders here.
header = "..."
footer = "..."
backoff_entity = "..."
recv_station = "..."
send_station = "..."

def gen_backoff_entities(n):
    out = ""
for i in range(n):
    out += backoff_entity.replace("AC_PLACEHOLDER", str(i + 1))
return out

def gen_stations(n):
    backoff_entities = gen_backoff_entities(1)
    out = recv_station
    .replace("MAC_PLACEHOLDER", str(1))
    .replace("BACKOFF_ENTITY_PLACEHOLDER", backoff_entities)

    for i in range(n):
        out += send_station
        .replace("MAC_PLACEHOLDER", str(i + 2))
        .replace("BACKOFF_ENTITY_PLACEHOLDER", backoff_entities)
        .replace("DA_PLACEHOLDER", str(1))
        .replace("AC_PLACEHOLDER", str(1))
return out

def main():
    i = 5 # number of stations
    scenario = header + gen_stations(i) + footer
    f = open("a3-" + str(i) + ".xml", "w")
    1f.write(scenario)
    f.close()

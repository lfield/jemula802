java -Xmx500m \
    -classpath "jemula802/bin:jemula/bin:jemula/lib/xstream-1.3.1.jar:jemula/lib/jfreechart-1.5.3.jar:jemula/lib/jcommon-1.0.23.jar:jemula/lib/commons-io-2.6.jar" \
    emulator.JE802Starter \
    $1

# example:
#  ./jemula802/resources/run_Jemula802_on_cygwin.sh jemula802/scenarios/examples/Montana.xml

java -Xms2G -Xmx2G -classpath "./;jemula802/bin;jemula/bin;jemula/lib/xstream-1.3.1.jar;jemula/lib/jfreechart-1.5.3.jar;jemula/lib/jcommon-1.0.23.jar" emulator.JE802Starter $1

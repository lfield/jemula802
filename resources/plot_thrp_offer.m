function plot_thrp_offer()

total_offer; total_thrp;

fig_handle = figure('NumberTitle', 'on', 'Name', 'all','PaperPositionMode', 'auto','Pointer', 'arrow');

set(gcf, 'color', 1/255 * [214 221 225]);
set(gcf, 'InvertHardCopy', 'off');
set(gcf,'Renderer','painters');

set(fig_handle,'Position', [200 20 1000 600]);
set(gca,'Position',[0.08,0.14,0.9,0.82]);
set(gca,'FontSize',22);
box on;
axis on;

set(gca,"xminortick", "on")
set(gca,"xminorgrid", "on")

#ylim([0 10]);
xlim([0 max(result_total_offer(:,1)/1000)]*1.05);

ylabel('Simulation Results [Mb/s]');
xlabel('Simulated Time [s]');
grid on;
hold on;

plot(result_total_offer(:,1)/1000,result_total_offer(:,7),'-','Color',[.6 .6 .8],'LineWidth',2);
plot(result_total_thrp(:,1)/1000,result_total_thrp(:,7),'-','Color','k','LineWidth',1);

l_handle = legend("Offer [Mb/s]","Throughput [Mb/s]");
set (l_handle, "FontSize", 22, "Color", [0.9 0.9 0.9], "Location", "SouthWest");

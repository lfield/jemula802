function plot_pdf_cdf(data)

  aMax = max(data);
  aMin = min(data);
  aNumberOfSamples = size(data,2);


  %% --- show a delay histogram plot
  figName = ('Results_Histogram');
  figure('NumberTitle', 'on', 'Name', figName,'PaperPositionMode', 'auto','Position', [540 400 800 500],'Pointer', 'arrow');

  box on;
  axis on;
  grid on;

  aLineWidth = 1;
  aMarkerSize = 6;
  FontSize = 16;
  set(gcf, 'color', 'white');
  set(gcf, 'InvertHardCopy', 'off');
  set(gca,'FontSize',FontSize);
  set(gca,'FontName','Roboto');
  set(gca,'Position',[0.15,0.14,0.8,0.82]);
  hold on;

  aNumOfBins = 50;
  aBinWidth = (max(data)-min(data)) / aNumOfBins;
  theBins = zeros(1,aNumOfBins);

  aBin = 0;
  for cnt = 1 :size(data,2) % loop through data to feed the samples into the bins
    sample = data(cnt);
    aBin = 1 + round(((sample - min(data)) / (max(data)-min(data))) * (aNumOfBins-1)); % decide into which bin this sample falls.
    theBins(aBin) = theBins(aBin) + 1;
  endfor

  theBins = theBins ./ sum(theBins); % number of occurances -> probabilities
  h = plot (min(data)+aBinWidth .* [1:aNumOfBins],1-cumsum(theBins));
  set(h,'Color','b','LineWidth',aLineWidth,'Marker','+','MarkerSize',aMarkerSize);
  h = plot (min(data)+aBinWidth .* [1:aNumOfBins],theBins);
  set(h,'Color','k','LineWidth',aLineWidth,'Marker','o','MarkerSize',aMarkerSize);

  xlabel('x','FontSize',FontSize);
  ylabel('apprx. probabilities (CDF:sample>x; PDF:sample=x)','FontSize',FontSize);
  xlim([aMin-((aMax-aMin)/20) , aMax + ((aMax-aMin)/20)]);
  h=legend ('CDF', 'PDF');
  set (h,'FontSize',FontSize);
  warning ("off", "Octave:negative-data-log-axis");
  set(gca,'YScale','log')
  warning ("off", "Octave:axis-non-positive-log-limits");
  ylim([min(ylim) 1.1]);

  endfunction

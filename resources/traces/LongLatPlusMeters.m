## Copyright (C) 2020 smangold
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} LongLatPlusMeters (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: smangold <smangold@SKYNODE-LENOVO>
## Created: 2020-10-18

function retval = LongLatPlusMeters (latitude, longitude, dx, dy)

#latitude_example = 47.4582167;
#longitude_example = 008.5554750;

r_earth = 6378000;

new_latitude  = latitude  + (dy / r_earth) * (180 / pi);
new_longitude = longitude + (dx / r_earth) * (180 / pi) / cos(latitude * pi/180);

retval =[new_latitude' new_longitude'];

endfunction
